| | |
| - | - |
| Files | * |
| Copyright | 2024, Dexcube |
| License | Expat |

Comment:\
 Copyright (c) 2024 266-750Balloons, Dexcube

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

| | |
| - | - |
| Files | resources/colors.less |
| Copyright | 2020-2024, Xiphoseer, 2024 Dexcube |
| License | MPL-2.0 |

Comment:\
 I am using the color pallettes from this project for the time being. I transcribed them into a LESS stylesheet,  and am attributing them to Xiphoseer, as it is derived from his work. [Read the license here.](https://github.com/Xiphoseer/htmlcars/blob/main/LICENSE)

| | |
| - | - |
| Files | resources/Antonio/* |
| Copyright | 2013, Vernon Adams |
| License | OFL-1.1 |

Comment:\
 This font looks very LCARS-like. I have included these fonts so they can be served from a local rather than a Google server.

| | |
| - | - |
| Files | resources/Open_Sans/* |
| Copyright | 2011, Steven Matteson |
| License | OFL-1.1 |

Comment:\
 Once again, here to be served locally rather than from the Googs.