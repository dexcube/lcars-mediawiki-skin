# Library Computer Access and Retrieval Skin (LCARS)
## What?
This is an experimental Mediawiki skin styled to look like the computer systems in the **Star Trek** franchise, developed for Mediawiki 1.41 and 1.42.
Please note that this skin is very new and not suitable for production use (yet). It's rough around the edges, with lots of features missing and the styles needing A LOT of work. With that said, here's how it's looking right now:
![A screenshot of MediaWiki instance on the Main Page with a skin that looks like an LCARS panel from the Star Trek franchise. The user is logged in as Admin, and the Main Page is filed with irrelevant test text.](assets/README%20Images/lcars%20test%20wiki%20state%207-3-2024.png)
Here's what works:
* Basic Page Viewing and Navigation
* Editing
* Visual Editor (kind of sort of. When you press "Save Changes", the dialog will not dissappear, but if you reload the page, your edits are there.)
* Account Preferences

And here's what I'm still missing:
* A working Edit button in a sensible location (That weird one at the top doesn't work and is just to keep VisualEditor from whining)
* Search
* Site Banner Support
* Page Administration/Tools
* Probably other stuff I'm not thinking of and will run into as I try to use this, as I am very new to Mediawiki development.

In addition, in the future, after the base skin is done, I want to add infrastructure for color scheme support (e.g Voyager, Cerritos, custom, etcetera) and maybe even (opt-in) sounds! The former will probably just involve a script to generate multiple skins, with the latter entailing some sort of control built into the (not yet extant) Javascript. Maybe (No promises!) I'll write an extension that allows a user to set their own colors and sounds.

## When?
There's a good chance I'll get busy and never finish this, and that this repo will sit as as empty tomb to broken promises, but we can always hope for otherwise!

## Why?
On a basic level, I want to make a cool MediaWiki skin. In addition, I hope to use this as a component to make a new Star Trek fan wiki family independent of Fandom Wiki (although that is a pie-in-the-sky fantasy) as well as a component for other Star Trek fan projects.

## How?
### Development
In short, lots of blood, sweat, tears, torturing JSON, learning Mustache templates, and reading cryptic MediaWiki documentation (no offense intended. MediaWiki is such a large codebase that I'm sure it's an absolute BEHEMOTH to document! I'm sure I could do much worse.). Luckily, LESS is pretty nice. I designed a lot of LCARS assets like corners and end caps in Inkscape and copied the SVG code into their own Mustache templates.

### Installation
Remeber that this is an experimental skin. You should be able to change it back from user settings, but no guarantees. This only working with 1.41 and 1.42 (with no guarantee of support for either, but I just upgraded my dev instance to 1.42 so I'm not testing it on 1.41 for now.)
1. Add this skin to your skin folder
```bash
cd /var/www/html/mediawiki/skins # Change this to wherever your wiki's skin folder is located
git clone https://gitlab.com/dexcube/lcars-mediawiki-skin LCARS # The folder name must be LCARS for font resource loads to work correctly
```
2. Open your LocalSettings.php and add the skin by adding this line.
```
wfLoadSkin( 'LCARS' );
```
Hopefully, one day, I'll just set up a proper script to generate releases, but this is the process for now.
## License
This project is licensed under the MIT/Expat license, except for a a few files. The first is a LESS file with color definitions derived from Xiphoseer's [HTMLCARS](https://github.com/Xiphoseer/HTMLCARS) and is thus licensed under the MPL-2.0. [Click here for the full info](LICENSE.md). 

## Credits
* Xiphoseer's [HTMLCARS](https://github.com/Xiphoseer/HTMLCARS), from which I am using the color pallettes.
* The Wikimedia Foundation, for making the least terrible wiki software that is actually maintained and useful for more than the simplest use cases.
* All the cool people that work on *Star Trek*. You bring much glory to your houses.